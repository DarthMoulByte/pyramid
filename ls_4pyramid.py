#!BPY

"""
Name: '4-Pyramid'
Blender: 249
Group: 'AddMesh'
"""
__author__= ['Luis Sergio S. Moura Jr.']
__url__ = ("http://www.icetempest.com")
__version__= '0.1'
__bpydoc__= '''\

4-Pyramid Mesh

This script lets the user create a pyramid. Using only rectangles (no paraleograms).
Receives as input the number of steps, height of each step and width of each step.

0.1 - 2009-06-30 by Luis Sergio<br />
- initial version
'''

# *** BEGIN LICENSE BLOCK ***
# Creative Commons 3.0 by-sa
#
# Full license: http://creativecommons.org/licenses/by-sa/3.0/
# Legal code: http://creativecommons.org/licenses/by-sa/3.0/legalcode
#
# You are free:
# * to Share  to copy, distribute and transmit the work
# * to Remix  to adapt the work
#
# Under the following conditions:
# * Attribution. You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
# * Share Alike. If you alter, transform, or build upon this work, you may distribute the resulting work only under the same, similar or a compatible license.
#
# * For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.
# * Any of the above conditions can be waived if you get permission from the copyright holder.
# * Nothing in this license impairs or restricts the author's moral rights.
# *** END LICENSE BLOCK ***

# Importing modules
import BPyAddMesh
import Blender

import BPyAddMesh
import Blender

def MakeQuads2(LineH, LineL):
	h = LineH
	l = LineL
	h.append(h[0])
	l.append(l[0])
	faces = []
	for i in range(len(h) - 1):
		f = [h[i], h[i+1],l[i+1],l[i]]
		faces.append(f)
	return(faces)

def MakeQuads(num, offsetH, offsetL):
	h = range(num)
	l = range(num)
	for i in range(len(h)):
		h[i] = h[i] + offsetH
		l[i] = l[i] + offsetL
	return(MakeQuads2(h, l))

# Links the previous level vertices with this level vertices
# making a lot of quads
# ThisStart = in wich vertice THIS LEVEL starts
def MakeHQuads(LEVEL, ThisStart, PrevStart):
	mult = LEVEL * 2
	# Number of faces in this level
	facenum = LEVEL * 2 * 4
	k = 0
	faces = []
	offset = 0
	
	for i in range(facenum):
		if (i % mult == 0):
			lv = ThisStart + offset + i - 1
			# The first one needs special care
			if (i == 0):
				lv = ThisStart + facenum + 3
			cv = ThisStart + offset + i
			f = [PrevStart + k, lv, cv, cv+1]
			offset = offset + 1
		else:
			fv = PrevStart + k + 1
			# Checks if we're past the last vertice, and start over
			if (fv == ThisStart):
				fv = PrevStart
			f = [PrevStart + k, fv, ThisStart + offset + i, ThisStart + offset + i - 1]
			k = k + 1
		faces.append(f)
	print(faces)
	return(faces)
	

def FourPyramid(STEPS, TOPSIDE = 1.0, STEPHEIGHT = 1.0):
    verts = []
    faces = []

    d = TOPSIDE / 2.0
    h = STEPS * STEPHEIGHT

    # High vertices (of the current step)
    hverts = []
    # Low vertices (of the current step)
    lverts = []
    thisStart = 0
    prevStart = 0
    # Iterating each step...
    for i in range(STEPS):
        thisStart = len(verts)
        hverts_old = hverts
        lverts_old = lverts
        hverts = []
        lverts = []
        if (i == 0):
            v = [-d,  d, h ]
            hverts.append(v)
            v = [ d,  d, h ]
            hverts.append(v)
            v = [ d, -d, h ]
            hverts.append(v)
            v = [-d, -d, h ]
            hverts.append(v)

            v = [-d,  d, h - STEPHEIGHT ]
            lverts.append(v)
            v = [ d,  d, h - STEPHEIGHT ]
            lverts.append(v)
            v = [ d, -d, h - STEPHEIGHT ]
            lverts.append(v)
            v = [-d, -d, h - STEPHEIGHT ]
            lverts.append(v)
            
            faces.append([0, 1, 2, 3])
            faces.append([0, 1, 5, 4])
            faces.append([1, 2, 6, 5])
            faces.append([2, 3, 7, 6])
            faces.append([3, 0, 4, 7])
        else:
            vertnum = (i * 2 + 1) * 4
            # this is the maximum distance from the center in this step
            cd = d * (i * 2) + d
            #print("cd(%d) = %d" % (i, cd))
            #print("Level %d has %d vertices per level" % (i, vertnum))
            # Each side
            for j in range(4):
                kd = -cd
                # Each vertex
                for k in range(i * 2 + 1):
                    print("kd: %f\td:%f" % (kd,d))
                    #kd = 2 * d * k - cd
                    if (j == 0):
                        vh = [kd, cd, h]
                        vl = [kd, cd, h - STEPHEIGHT ]
                    elif (j == 1):
                        vh = [cd, -kd, h]
                        vl = [cd, -kd, h - STEPHEIGHT ]
                    elif (j == 2):
                        vh = [-kd, -cd, h]
                        vl = [-kd, -cd, h - STEPHEIGHT ]
                    else:
                        vh = [-cd, kd, h]
                        vl = [-cd, kd, h - STEPHEIGHT ]
                    hverts.append(vh)
                    lverts.append(vl)
                    kd = kd + 2 * d
            f = MakeQuads(vertnum, len(verts), len(verts) + vertnum)
            for x in f:
              faces.append(x)
            f = MakeHQuads(i, thisStart, prevStart)
            for x in f:
              faces.append(x)

                    
        #d = d + TOPSIDE
        h = h - STEPHEIGHT

        prevStart = thisStart + len(hverts)



        for v in hverts:
            verts.append(v)
        for v in lverts:
            verts.append(v)

    return((verts,faces))

def DrawDefault():
	pyramidSteps = Blender.Draw.Create(10)
	PyramidHSize = Blender.Draw.Create(1.0)
	PyramidWSize = Blender.Draw.Create(0.5)
	
	block = []
	block.append(("Steps:", pyramidSteps, 2, 100, "the number of steps"))
	block.append(("Step Height:", PyramidHSize, 0.01, 100, "height of each step"))
	block.append(("Step Width:", PyramidWSize, 0.01, 100, "width of the steps"))
	
	if not Blender.Draw.PupBlock("Create pyramid", block):
		return
	
#	Generates the mesh
	verts, faces = FourPyramid(pyramidSteps.val, PyramidHSize.val, PyramidWSize.val)

#	Adds the mesh to the scene
	BPyAddMesh.add_mesh_simple('4Pyramid', verts, [], faces)

def DrawHeightBase():
	pyramidSteps = Blender.Draw.Create(20)
	pyramidBaseSize = Blender.Draw.Create(20.0)
	PyramidHeight = Blender.Draw.Create(10.0)
	PyramidSideAngle = Blender.Draw.Create(90)

	block = []
	block.append(("Sides:", pyramidSides, 3, 100, "the number of sides"))
	block.append(("Steps:", pyramidSteps, 2, 1000, "the number of steps"))
	block.append(("Height:", PyramidHeight, 0.01, 1000, "height of the pyramid"))
	block.append(("Step Angle:", PyramidSideAngle, 1, 179, "angle of the sides"))

	if not Blender.Draw.PupBlock("Create pyramid", block):
		return
	
	# ANGLE = pyramid.PI * 2 / pyramidSides.val * 2
	ANGLE = PI / pyramidSides.val

	STEPW = pyramidBaseSize.val / (2 * pyramidSteps.val * math.tan(ANGLE))
	STEPH = PyramidHeight.val / pyramidSteps.val
	
#	Generates the mesh
	verts, faces = add_pyramid(pyramidSteps.val, STEPH, STEPW)

#	Adds the mesh to the scene
	BPyAddMesh.add_mesh_simple('4Pyramid', verts, [], faces)

def DrawAngleBase():
	pyramidSteps = Blender.Draw.Create(20)
	pyramidBaseSize = Blender.Draw.Create(20.0)
	PyramidAngle = Blender.Draw.Create(52)

	block = []
	block.append(("Sides:", pyramidSides, 3, 100, "the number of sides"))
	block.append(("Steps:", pyramidSteps, 2, 1000, "the number of steps"))
	block.append(("Angle:", PyramidAngle, 1, 89, "height of the pyramid"))

	if not Blender.Draw.PupBlock("Create pyramid", block):
		return
	
	# ANGLE = PI * 2 / pyramidSides.val * 2
	ANGLE = PI / pyramidSides.val
	STEPW = pyramidBaseSize.val / (2 * pyramidSteps.val * math.tan(ANGLE))
	PYRAMIDANGLE = PyramidAngle.val * PI / 180
        HEIGHT = math.tan(PYRAMIDANGLE) * STEPW * pyramidSteps.val
	STEPH = HEIGHT / pyramidSteps.val

#	Generates the mesh
	verts, faces = add_pyramid(pyramidSteps.val, STEPH, STEPW)

#	Adds the mesh to the scene
	BPyAddMesh.add_mesh_simple('4Pyramid', verts, [], faces)        

# main function (window handle and input variables)
def main():
	ptype = Blender.Draw.PupMenu("What type of Pyramid?%t|Default|Height x Base|Angle x Base")
	if (ptype == 1):
                DrawDefault()
        elif (ptype == 2):
                DrawHeightBase()
        elif (ptype == 3):
                DrawAngleBase()
        

# call our main function
if __name__ == "__main__":
        main()

