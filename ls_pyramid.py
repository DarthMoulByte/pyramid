#!BPY

"""
Name: 'N-Pyramid'
Blender: 249
Group: 'AddMesh'
"""
__author__= ['Luis Sergio S. Moura Jr.']
__url__ = ("http://www.icetempest.com")
__version__= '0.2'
__bpydoc__= '''\

N-Pyramid Mesh

This script lets the user create a new primitive: A pyramid. Receives as input the 
number of sides, number of steps, height of each step and distance of each step.

0.1 - 2009-06-25 by Luis Sergio<br />
- initial version

0.2 - 2009-06-26 by Luis Sergio<br />
- added side angle feature.
- added multiple types of parameter input (Thank you for your suggestion, RickyBlender!).
'''

# *** BEGIN LICENSE BLOCK ***
# Creative Commons 3.0 by-sa
#
# Full license: http://creativecommons.org/licenses/by-sa/3.0/
# Legal code: http://creativecommons.org/licenses/by-sa/3.0/legalcode
#
# You are free:
# * to Share  to copy, distribute and transmit the work
# * to Remix  to adapt the work
#
# Under the following conditions:
# * Attribution. You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
# * Share Alike. If you alter, transform, or build upon this work, you may distribute the resulting work only under the same, similar or a compatible license.
#
# * For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.
# * Any of the above conditions can be waived if you get permission from the copyright holder.
# * Nothing in this license impairs or restricts the author's moral rights.
# *** END LICENSE BLOCK ***

# Importing modules
import BPyAddMesh
import Blender
import math

PI = 3.1415926535897932384626433832795

# This generates as many faces as needed to close up a sequence of vertices.
def faceGen(SIDES, offset = 0):
        faces = []
        if (SIDES % 2 == 0):
                # Pair number of faces. Let's make quads.
                start = 0
                end = SIDES - 1
                while (start < (end-1)):
                        faces.append([start + offset, start+1 + offset, end-1 + offset, end + offset])
                        start = start + 1
                        end = end - 1
        else:
                # Odd number of faces. Triangles it is!
                start = 0
                end = SIDES - 1
                while (start < (end - 1)):
                        faces.append([start + offset, start + 1 + offset, end + offset])
                        start = start + 1
                        if (start < (end - 1)):
                                faces.append([end - 1 + offset, end + offset, start + offset,])
                                end = end - 1
        return(faces)

def add_pyramid(SIDES, STEPS, HSIZE, WSIZE, ANGLE = 90):
	verts = []
	faces = []

	rot_step = 2 * PI / SIDES
	h = STEPS * HSIZE
	# How much to reduce the step so we have the required angle
	angle_red = 0
	if (ANGLE != 90):
                rad_angle = ANGLE * PI / 180
                angle_red = HSIZE / math.tan(rad_angle)
                print("Reduced: %.2f (sin: %.2f cos: %.2f)" % (angle_red, math.sin(rad_angle), math.cos(rad_angle)))
                print("Angle: %.2f = %.2f rad" % (ANGLE, rad_angle))

	for i in range(STEPS):
                # Helper vertex groups
                v0 = [] # Previous group of vertexes
                v1 = [] # First group of vertexes for this level
                v2 = [] # Second group of vertexes for this level
                for j in range(SIDES):
                        v = 2 * i * SIDES + j
                        v0.append(v - SIDES)
                        v1.append(v)
                        v2.append(v + SIDES)
                v0.append(2 * i * SIDES - SIDES)
                v1.append(2 * i * SIDES)
                v2.append(2 * i * SIDES + SIDES)
                
                # Create CAP
                for j in range(SIDES):
                        vx = (i+1) * WSIZE * math.cos(rot_step * j)
                        vy = (i+1) * WSIZE * math.sin(rot_step * j)
                        dx = angle_red * math.cos(rot_step * j)
                        dy = angle_red * math.sin(rot_step * j)

                        vx = vx - dx
                        vy = vy - dy
                        
                        v = [vx, vy, h]
                        verts.append(v)

                # Create CAP Face(s)
                if (i == 0):
                        if (SIDES <= 4):
                                face = []
                                for j in range(SIDES):
                                        face.append(j)
                                faces.append(face)
                        else:
                                topfaces = faceGen(SIDES)
                                for f in topfaces:
                                        faces.append(f)
                else:
                        for j in range(SIDES):
                                face = [v1[j], v1[j+1], v0[j+1], v0[j]]
                                faces.append(face)
                        
                # Down one step
                h = h - HSIZE

                # Bottom side
                for j in range(SIDES):
                        vx = (i+1) * WSIZE * math.cos(rot_step * j)
                        vy = (i+1) * WSIZE * math.sin(rot_step * j)
                        v = [vx, vy, h]
                        verts.append(v)

                # Create SIDE Faces
                for j in range(SIDES):
                        face = [v2[j], v2[j+1], v1[j+1], v1[j]]
                        faces.append(face)

        return verts,faces


def DrawDefault():
	pyramidSides = Blender.Draw.Create(4)
	pyramidSteps = Blender.Draw.Create(10)
	PyramidHSize = Blender.Draw.Create(1.0)
	PyramidWSize = Blender.Draw.Create(0.5)
	PyramidAngle = Blender.Draw.Create(90)
	
	block = []
	block.append(("Sides:", pyramidSides, 3, 100, "the number of sides"))
	block.append(("Steps:", pyramidSteps, 2, 100, "the number of steps"))
	block.append(("Step Height:", PyramidHSize, 0.01, 100, "height of each step"))
	block.append(("Step Width:", PyramidWSize, 0.01, 100, "width of the steps"))
	block.append(("Step Angle:", PyramidAngle, 1, 179, "angle of the sides"))
	
	if not Blender.Draw.PupBlock("Create pyramid", block):
		return
	
#	Generates the mesh
	verts, faces = add_pyramid(pyramidSides.val, pyramidSteps.val, PyramidHSize.val, PyramidWSize.val, PyramidAngle.val)

#	Adds the mesh to the scene
	BPyAddMesh.add_mesh_simple('NPyramid', verts, [], faces)

def DrawHeightBase():
	pyramidSides = Blender.Draw.Create(4)
	pyramidSteps = Blender.Draw.Create(20)
	pyramidBaseSize = Blender.Draw.Create(20.0)
	PyramidHeight = Blender.Draw.Create(10.0)
	PyramidSideAngle = Blender.Draw.Create(90)

	block = []
	block.append(("Base Size:", pyramidBaseSize, 1.0, 1000.0, "the size of the one side of the base"))
	block.append(("Sides:", pyramidSides, 3, 100, "the number of sides"))
	block.append(("Steps:", pyramidSteps, 2, 1000, "the number of steps"))
	block.append(("Height:", PyramidHeight, 0.01, 1000, "height of the pyramid"))
	block.append(("Step Angle:", PyramidSideAngle, 1, 179, "angle of the sides"))

	if not Blender.Draw.PupBlock("Create pyramid", block):
		return
	
	# ANGLE = pyramid.PI * 2 / pyramidSides.val * 2
	ANGLE = PI / pyramidSides.val

	STEPW = pyramidBaseSize.val / (2 * pyramidSteps.val * math.tan(ANGLE))
	STEPH = PyramidHeight.val / pyramidSteps.val
	
#	Generates the mesh
	verts, faces = add_pyramid(pyramidSides.val, pyramidSteps.val, STEPH, STEPW, PyramidSideAngle.val)

#	Adds the mesh to the scene
	BPyAddMesh.add_mesh_simple('NPyramid', verts, [], faces)

def DrawAngleBase():
	pyramidSides = Blender.Draw.Create(4)
	pyramidSteps = Blender.Draw.Create(20)
	pyramidBaseSize = Blender.Draw.Create(20.0)
	PyramidAngle = Blender.Draw.Create(52)
	PyramidSideAngle = Blender.Draw.Create(90)

	block = []
	block.append(("Base Size:", pyramidBaseSize, 1.0, 1000.0, "the size of the one side of the base"))
	block.append(("Sides:", pyramidSides, 3, 100, "the number of sides"))
	block.append(("Steps:", pyramidSteps, 2, 1000, "the number of steps"))
	block.append(("Angle:", PyramidAngle, 1, 89, "height of the pyramid"))
	block.append(("Step Angle:", PyramidSideAngle, 1, 179, "angle of the sides"))

	if not Blender.Draw.PupBlock("Create pyramid", block):
		return
	
	# ANGLE = PI * 2 / pyramidSides.val * 2
	ANGLE = PI / pyramidSides.val
	STEPW = pyramidBaseSize.val / (2 * pyramidSteps.val * math.tan(ANGLE))
	PYRAMIDANGLE = PyramidAngle.val * PI / 180
        HEIGHT = math.tan(PYRAMIDANGLE) * STEPW * pyramidSteps.val
	STEPH = HEIGHT / pyramidSteps.val

#	Generates the mesh
	verts, faces = add_pyramid(pyramidSides.val, pyramidSteps.val, STEPH, STEPW, PyramidSideAngle.val)

#	Adds the mesh to the scene
	BPyAddMesh.add_mesh_simple('NPyramid', verts, [], faces)        

# main function (window handle and input variables)
def main():

	ptype = Blender.Draw.PupMenu("What type of Pyramid?%t|Default|Height x Base|Angle x Base")
	if (ptype == 1):
                DrawDefault()
        elif (ptype == 2):
                DrawHeightBase()
        elif (ptype == 3):
                DrawAngleBase()
        

# call our main function
if __name__ == "__main__":
        main()

